// Copyright © 2021 Finn Herzfeld <finn@janky.solutions>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package send

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"

	"gitlab.com/signald/signald-go/cmd/signaldctl/common"
	"gitlab.com/signald/signald-go/cmd/signaldctl/config"
	v1 "gitlab.com/signald/signald-go/signald/client-protocol/v1"
)

const (
	CAPTCHA_HELPER = "signal-captcha-helper"
)

var (
	account       string
	toAddress     *v1.JsonAddress
	toGroup       string
	attachments   []string
	message       string
	captchaHelper bool

	SendMessageCmd = &cobra.Command{
		Use:   "send {group id | phone number} [message]",
		Short: "send a message",
		PreRun: func(cmd *cobra.Command, args []string) {
			if account == "" {
				account = config.Config.DefaultAccount
			}
			if account == "" {
				common.Must(cmd.Help())
				log.Fatal("No account specified. Please specify with --account or set a default")
			}
			if len(args) < 1 {
				common.Must(cmd.Help())
				log.Fatal("must specify both destination (either group id or phone number)")
			}

			if len(args) == 1 {
				messageBytes, err := ioutil.ReadAll(os.Stdin)
				if err != nil {
					log.Println("error reading message from stdin, perhaps you meant to include it in the command line arguments?")
					panic(err)
				}
				message = string(messageBytes)
			} else {
				message = strings.Join(args[1:], " ")
			}
			to, err := common.StringToAddress(args[0])
			if err != nil {
				toGroup = args[0]
			} else {
				toAddress = &to
			}
		},
		Run: func(_ *cobra.Command, args []string) {
			go common.Signald.Listen(nil)

			req := v1.SendRequest{
				Username:         account,
				MessageBody:      message,
				Attachments:      []*v1.JsonAttachment{},
				RecipientAddress: toAddress,
				RecipientGroupID: toGroup,
			}

			for _, attachment := range attachments {
				path, err := filepath.Abs(attachment)
				if err != nil {
					log.Fatal("error resolving attachment", err)
				}
				log.Println(path)
				req.Attachments = append(req.Attachments, &v1.JsonAttachment{Filename: path})
			}

			resp, err := req.Submit(common.Signald)
			if err != nil {
				log.Fatal("error sending request to signald: ", err)
			}

			resends := []*v1.JsonAddress{}

			for _, result := range resp.Results {
				if result.ProofRequiredFailure != nil {
					if captchaHelper {
						err = runCaptchaHelper(result.ProofRequiredFailure.Token)
						if err != nil {
							log.Println("error running captcha helper: ", err)
						}
						resends = append(resends, result.Address)
					}
				}
			}

			if len(resends) > 0 {
				resendReq := v1.SendRequest{
					Username:         req.Username,
					MessageBody:      req.MessageBody,
					Attachments:      req.Attachments,
					RecipientGroupID: req.RecipientGroupID,
					Members:          resends,
					Timestamp:        resp.Timestamp,
				}
				resendResponse, err := resendReq.Submit(common.Signald)
				if err != nil {
					log.Println("error resending messages: ", err)
				} else {
					for i, originalResult := range resp.Results {
						if originalResult.ProofRequiredFailure == nil {
							continue
						}

						for _, result := range resendResponse.Results {
							if result.Address.UUID == originalResult.Address.UUID {
								resp.Results[i] = result
							}
						}
					}
				}
			}

			switch common.OutputFormat {
			case common.OutputFormatJSON:
				err := json.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatYAML:
				err := yaml.NewEncoder(os.Stdout).Encode(resp)
				if err != nil {
					log.Fatal(err, "error encoding response to stdout")
				}
			case common.OutputFormatCSV, common.OutputFormatTable, common.OutputFormatDefault:
				t := table.NewWriter()
				t.SetOutputMirror(os.Stdout)
				t.AppendHeader(table.Row{"Number", "UUID", "Duration", "Send Error"})
				for _, result := range resp.Results {
					if result.Success != nil {
						t.AppendRow(table.Row{
							result.Address.Number,
							result.Address.UUID,
							fmt.Sprintf("%dms", result.Success.Duration),
							"",
						})
					} else {
						var sendError string
						if result.IdentityFailure != "" {
							sendError = fmt.Sprintf("identity failure: %s\n", result.IdentityFailure)
						}
						if result.NetworkFailure {
							sendError = "network failure"
						}
						if result.UnregisteredFailure {
							sendError = "user not registered"
						}
						t.AppendRow(table.Row{result.Address.Number, result.Address.UUID, "", sendError})
					}
				}

				if common.OutputFormat == common.OutputFormatCSV {
					t.RenderCSV()
				} else {
					common.StylizeTable(t)
					t.Render()
				}
			default:
				log.Fatal("Unsupported output format")
			}
		},
	}
)

func runCaptchaHelper(challenge string) error {
	if !captchaHelper {
		return nil
	}

	_, err := exec.LookPath(CAPTCHA_HELPER)
	if err != nil {
		return err
	}

	cmd := exec.Command(CAPTCHA_HELPER, "--challenge")

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}

	err = cmd.Start()
	if err != nil {
		return err
	}

	captchaToken := new(strings.Builder)
	_, err = io.Copy(captchaToken, stdout)
	if err != nil {
		return err
	}

	err = cmd.Wait()
	if err != nil {
		return err
	}

	req := v1.SubmitChallengeRequest{
		Account:      account,
		CaptchaToken: captchaToken.String(),
		Challenge:    challenge,
	}

	err = req.Submit(common.Signald)
	if err != nil {
		return err
	}

	return nil
}

func init() {
	SendMessageCmd.Flags().StringVarP(&account, "account", "a", "", "local account to use")
	SendMessageCmd.Flags().BoolVarP(&captchaHelper, "captcha-helper", "c", false, "Invoke signal-captcha-helper and process the response when a push challenge response appears. After completing the challenge, the message will be redelivered to the failed recipient")
	SendMessageCmd.Flags().StringSliceVarP(&attachments, "attachment", "A", []string{}, "attach a file to your outbound message. may be specified multiple times.")
}
